#!/usr/bin/env bash

echo "Updating kf5 from $1"

find elements -iname '*.bst' -exec sed -i "s/kde_framework:$1/kde_framework:5.102/g" {} \;
bst track `echo elements/kf5/* | sed 's/elements\///g'`